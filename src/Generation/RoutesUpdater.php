<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp\Generation;

use OpenapiNextGeneration\GenerationHelperPhp\TargetDirectory;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Route;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\RoutesSpecification;

class RoutesUpdater
{
    protected $controllerBuilder;
    protected $routes;
    protected $specHash;

    public function __construct(ControllerBuilder $controllerBuilder = null)
    {
        if (!$controllerBuilder instanceof ControllerBuilder) {
            $controllerBuilder = new ControllerBuilder();
        }
        $this->controllerBuilder = $controllerBuilder;
    }

    public function updateRoutesFile(array $specification, string $file): void
    {
        $fileContent = file_get_contents($file);

        $usesToAdd = [];

        $routes = $this->getRoutesFromSpecification($specification);

        $cursor = strlen($fileContent) - 1;
        /* @var $route Route */
        foreach ($routes as $route) {
            $routePos = strpos($fileContent, $this->createRouteSignature($route));
            if ($routePos === false) {
                $insertPos = strpos($fileContent, "\n", $cursor);
                $newRoute = "\n" . $this->createRouteDefinition($route);
                $fileContent = substr($fileContent, 0, $insertPos) . $newRoute . substr($fileContent, $insertPos);
                $cursor = $insertPos + strlen($newRoute);

                $useCommand = $this->createUse($route);
                if (strpos($fileContent, $useCommand) === false) {
                    $usesToAdd[$useCommand] = $useCommand;
                }
            } else {
                $cursor = $routePos;
            }
        }

        $fileContent = $this->addUses($fileContent, $usesToAdd);

        file_put_contents($file, $fileContent);
    }

    public function updateControllers(array $specification, string $targetNamespace, string $targetDirectory = null): void
    {
        if ($targetDirectory === null) {
            $targetDirectory = TargetDirectory::getNamespaceDirectory($targetNamespace);
        }
        $targetDirectory = TargetDirectory::getCanonicalTargetDirectory($targetDirectory);

        $routes = $this->getRoutesFromSpecification($specification);

        $controllers = [];
        /* @var $route Route */
        foreach ($routes as $route) {
            $controllerName = $route->getUniqueResourceName() . 'Controller';
            if (!isset($controllers[$controllerName])) {
                $controllers[$controllerName] = [];
            }
            $controllers[$controllerName][strtolower($route->getHttpMethod())] = $route;
        }

        foreach ($controllers as $controllerName => $methods) {
            $fileName = $targetDirectory . $controllerName . '.php';
            $fileContents = @file_get_contents($fileName);
            if ($fileContents !== false) {
                $controllerContent = $this->controllerBuilder->build(
                    $targetNamespace,
                    $controllerName,
                    $methods,
                    $fileContents
                );
            } else {
                $controllerContent = $this->controllerBuilder->build(
                    $targetNamespace,
                    $controllerName,
                    $methods
                );
            }

            file_put_contents($fileName, $controllerContent);
        }
    }

    protected function getRoutesFromSpecification(array $specification): array
    {
        $specHash = md5(serialize($specification));
        if ($specHash != $this->specHash) {
            $routesSpecification = new RoutesSpecification($specification);
            $this->routes = $routesSpecification->getUniqueRoutes();
            $this->specHash = $specHash;
        }
        return $this->routes;
    }

    protected function addUses(string $fileContent, array $usesToAdd): string
    {
        preg_match_all('/use .*;/', $fileContent, $matches, PREG_OFFSET_CAPTURE);
        $lastUse = end($matches[0]);
        $lastUsePos = $lastUse[1] + strlen($lastUse[0]);
        $usesToAddString = '';
        foreach ($usesToAdd as $use) {
            $usesToAddString .= "\n" . $use;
        }

        return substr($fileContent, 0, $lastUsePos) . $usesToAddString . substr($fileContent, $lastUsePos);
    }

    protected function createRouteSignature(Route $route): string
    {
        return "'" . strtoupper($route->getHttpMethod()) . "', '" . $route->getPath() . "'";
    }

    protected function createUse(Route $route): string
    {
        return 'use App\\Controllers\\' . $route->getUniqueResourceName() . 'Controller;';
    }

    protected function createRouteDefinition(Route $route): string
    {
        return '$routes->addRoute(' . $this->createRouteSignature($route) . ', ' .
            $route->getUniqueResourceName() . "Controller::class . ':" . $route->getActionName() . "');";
    }
}