<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp\Generation;

use OpenapiNextGeneration\GenerationHelperPhp\TypeMapper;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Route;
use PhpParser\Builder\Param;
use PhpParser\BuilderFactory;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Use_;
use PhpParser\Parser;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;
use PhpParser\PrettyPrinterAbstract;

class ControllerBuilder
{
    protected $parser;
    protected $builder;
    protected $codePrinter;

    public function __construct(
        Parser $parser = null,
        BuilderFactory $builder = null,
        PrettyPrinterAbstract $codePrinter = null
    ) {
        $this->parser = $parser;
        $this->builder = $builder;
        $this->codePrinter = $codePrinter;

        if (!$this->parser instanceof Parser) {
            $this->parser = (new ParserFactory())->create(ParserFactory::PREFER_PHP7);
        }

        if (!$this->builder instanceof BuilderFactory) {
            $this->builder = new BuilderFactory();
        }

        if (!$this->codePrinter instanceof PrettyPrinterAbstract) {
            $this->codePrinter = new Standard();
        }
    }

    public function build(string $namespaceName, string $controllerName, array $methods, string $oldFileContent = null): string
    {
        if ($oldFileContent === null) {
            $ast = [];
        } else {
            $ast = $this->parser->parse($oldFileContent);
        }
        $namespace = $this->getNamespace($ast, $namespaceName);
        $controller = $this->getController($namespace, $controllerName);
        $this->ensureUses($namespace);
        /* @var $route Route */
        foreach ($methods as $route) {
            $method = $this->getMethod($controller, $route);
            $this->buildParams($method, $route);
        }

        return "<?php\n\n" . $this->codePrinter->prettyPrint([$namespace]);
    }

    protected function getNamespace(array $ast, string $namespace): Namespace_
    {
        if (empty($ast)) {
            return $this->builder->namespace($namespace)->getNode();
        } else {
            return reset($ast);
        }
    }

    protected function getController(Namespace_ $namespace, string $controllerName): Class_
    {
        $controller = null;
        foreach ($namespace->stmts as $statement) {
            if ($statement instanceof Class_) {
                return $statement;
            }
        }

        $class = $this->builder->class($controllerName)->getNode();
        $namespace->stmts[] = $class;
        return $class;
    }

    protected function ensureUses(Namespace_ $namespace)
    {
        $usesToAdd = [
            $this->builder->use('Psr\Container\ContainerInterface')->as('Container')->getNode(),
            $this->builder->use('Symfony\Component\HttpFoundation\Request')->getNode()
        ];
        $statements = [];
        foreach ($namespace->stmts as $statement) {
            if ($statement instanceof Use_) {
                foreach ($usesToAdd as $index => $useToAdd) {
                    foreach ($statement->uses as $use) {
                        if ($use->name->parts == $useToAdd->uses[0]->name->parts) {
                            unset($usesToAdd[$index]);
                        }
                    }
                }
            } elseif ($statement instanceof Class_) {
                foreach ($usesToAdd as $useToAdd) {
                    $statements[] = $useToAdd;
                }
            }
            $statements[] = $statement;
        }
        $namespace->stmts = $statements;
    }

    protected function getMethod(Class_ $controller, Route $route): ClassMethod
    {
        $methodName = $route->getActionName();
        $method = $controller->getMethod($methodName);
        if (!$method instanceof ClassMethod) {
            $method = new ClassMethod(
                $methodName,
                [
                    'flags' => Class_::MODIFIER_PUBLIC
                ]
            );
            $controller->stmts[] = $method;
        }

        return $method;
    }

    protected function buildParams(ClassMethod $method, Route $route)
    {
        $requestParam = new Param('request');
        $requestParam->setTypeHint('Request');

        $containerParam = new Param('container');
        $containerParam->setTypeHint('Container');

        $method->params = [
            $requestParam->getNode(),
            $containerParam->getNode()
        ];

        $parameters = $route->getMethodSpecification()['parameters'] ?? [];

        foreach ($parameters as $param) {
            if ($param['in'] === 'path') {
                $parameter = new Param($this->getParamCamelcaseName($param['name']));
                $parameter->setTypeHint(TypeMapper::mapType($param['schema']['type']));
                if (!isset($param['required']) || $param['required'] === false) {
                    $parameter->setDefault(null);
                }
                $method->params[] = $parameter->getNode();
            }
        }
    }

    protected function getParamCamelcaseName(string $param): string
    {
        return lcfirst(str_replace(['-', '_'], '', ucwords($param, '-_')));
    }
}