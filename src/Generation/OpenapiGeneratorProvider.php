<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp\Generation;

use OpenapiNextGeneration\ApiDocsGeneratorPhp\ApiDocsGenerator;
use OpenapiNextGeneration\EntityGeneratorPhp\EntityBuilder;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Native;
use OpenapiNextGeneration\JsonSchemaGeneratorPhp\JsonSchemaBuilder;
use OpenapiNextGeneration\JsonSchemaGeneratorPhp\OpenApi\JsonSchemaGenerator;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\PatternMapper;
use OpenapiNextGeneration\OpenapiResolverPhp\AllOfResolver;
use OpenapiNextGeneration\OpenapiResolverPhp\ReferenceResolver;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class OpenapiGeneratorProvider implements ServiceProviderInterface
{
    const API_SPECIFICATION = 'api-specification';
    const API_SPECIFICATION_FILE = 'docs/api.yml';

    public function register(Container $pimple)
    {
        $pimple[self::API_SPECIFICATION] = function () {
            $specificationFile = self::API_SPECIFICATION_FILE;

            $referenceResolver = new ReferenceResolver();
            $specification = $referenceResolver->resolveReference($specificationFile)->getValue();

            $specification = $referenceResolver->resolveAllReferences(
                $specification,
                $specificationFile
            );

            $allOfResolver = new AllOfResolver();
            $specification = $allOfResolver->resolveKeywordAllOf($specification);

            return $specification;
        };

        $pimple[PatternMapper::class] = function () {
            return new PatternMapper();
        };

        $pimple[ApiDocsGenerator::class] = function () {
            return new ApiDocsGenerator();
        };

        $pimple[JsonSchemaBuilder::class] = function () {
            return new JsonSchemaBuilder(new JsonSchemaGenerator());
        };

        $pimple[EntityBuilder::class] = function () {
            return new EntityBuilder(new Native());
        };

        $pimple[RoutesUpdater::class] = function () {
            return new RoutesUpdater();
        };
    }
}
