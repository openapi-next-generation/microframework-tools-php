<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp;

use FastRoute\Dispatcher;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;

class Application implements HttpKernelInterface, TerminableInterface
{
    protected $config;
    protected $container;

    public function __construct(array $config, ContainerInterface $container)
    {
        $this->config = $config;
        $this->container = $container;
    }

    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true): Response
    {
        /* @var $dispatcher Dispatcher */
        $dispatcher = $this->container->get(Dispatcher::class);
        $result = $dispatcher->dispatch($request->getMethod(), $request->getBaseUrl() . $request->getPathInfo());
        switch ($result[0]) {
            case Dispatcher::FOUND:
                $request->attributes->set('_route', $this->getMatchedRoute($request, $result[2]));
                $this->onRouteMatched($request);
                return $this->runCallback(
                    $result[1],
                    array_map(
                        function ($value) {
                            return urldecode($value);
                        },
                        $result[2]
                    ),
                    $request
                );
            case Dispatcher::NOT_FOUND:
                return new JsonResponse([], Response::HTTP_NOT_FOUND);
            case Dispatcher::METHOD_NOT_ALLOWED:
                return new JsonResponse([], Response::HTTP_METHOD_NOT_ALLOWED);
        }
    }

    public function terminate(Request $request, Response $response)
    {
        //override in your own application class
    }

    protected function onRouteMatched(Request $request)
    {
        //override in your own application class
    }

    protected function runCallback($callback, array $params, Request $request): Response
    {
        $params = array_merge(
            [
                $request,
                $this->container
            ],
            $params
        );
        if (is_callable($callback)) {
            return call_user_func_array($callback, $params);
        } elseif (is_string($callback)) {
            $components = explode(':', $callback);
            if (strpos($callback, '::') === false) {
                $components = [new $components[0], $components[1]];
            }
            return call_user_func_array($components, $params);
        }
    }

    protected function getMatchedRoute(Request $request, array $requestParams)
    {
        return str_replace(['{', '}'], '', $request->getPathInfo());
    }
}