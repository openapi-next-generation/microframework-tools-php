<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Console\Application;

class ConsoleApplicationProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple[Application::class] = function (Container $pimple) {
            $container = new \Pimple\Psr11\Container($pimple);
            $app = new Application();
            require 'src/console.php';

            return $app;
        };
    }
}
