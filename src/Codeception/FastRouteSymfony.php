<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp\Codeception;

use Codeception\Configuration;
use Codeception\Exception\ModuleConfigException;
use Codeception\Lib\Framework;
use Codeception\TestInterface;
use Pimple\Container;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class FastRouteSymfony extends Framework
{
    /**
     * @var HttpKernelInterface
     */
    protected $app;
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var Container
     */
    protected $pimple;

    protected $requiredFields = ['app'];

    public function _initialize()
    {
        if (!is_file(Configuration::projectDir() . $this->config['app'])) {
            throw new ModuleConfigException(__CLASS__, "Bootstrap file {$this->config['app']} not found");
        }
    }

    public function _before(TestInterface $test)
    {
        $this->loadApp();
        $this->client = new Client($this->app);

        if ($this->pimple instanceof Container) {
            $pimple = $this->pimple;
            $this->client->setOnRequestCallback(function (Request $request) use ($pimple) {
                $pimple[Request::class] = $request;
            });
        }
    }

    protected function loadApp()
    {
        $this->app = require Configuration::projectDir() . $this->config['app'];
        // if $app is not returned but exists
        if (isset($app) && $app instanceof HttpKernelInterface) {
            $this->app = $app;
        }
        if (!isset($this->app)) {
            throw new ModuleConfigException(__CLASS__, "\$app instance was not received from bootstrap file");
        }
        if (isset($container) && $container instanceof ContainerInterface) {
            $this->container = $container;
        }
        if (isset($pimple) && $pimple instanceof Container) {
            $this->pimple = $pimple;
        }
    }

    public function grabService($service)
    {
        return $this->container->get($service);
    }
}
