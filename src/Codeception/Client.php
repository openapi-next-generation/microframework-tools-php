<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp\Codeception;

use Symfony\Component\BrowserKit\Request as DomRequest;
use Symfony\Component\HttpKernel\Client as SymfonyClient;

class Client extends SymfonyClient
{
    protected $onRequestCallback;

    public function setOnRequestCallback(callable $callback)
    {
        $this->onRequestCallback = $callback;
    }

    protected function filterRequest(DomRequest $request)
    {
        $httpRequest = parent::filterRequest($request);

        if (is_callable($this->onRequestCallback)) {
            call_user_func($this->onRequestCallback, $httpRequest);
        }

        return $httpRequest;
    }
}