<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class RequestProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple[Request::class] = function () {
            return Request::createFromGlobals();
        };
    }
}