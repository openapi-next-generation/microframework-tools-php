<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp;

use JsonSchema\Validator;
use Symfony\Component\HttpFoundation\Request;

class ValidRequestBody
{
    protected $invalidCallback;

    public function __construct(callable $invalidCallback)
    {
        $this->invalidCallback = $invalidCallback;
    }

    public function getValidBody(Request $request): array
    {
        $requestBody = $request->getContent();

        $validationFile = 'docs/json-schema' .$request->attributes->get('_route') . '/'
            . strtolower($request->getMethod()) . '/request/schema.json';
        if (is_file($validationFile)) {
            $validator = new Validator();
            $validator->validate(json_decode($requestBody), ['$ref' => 'file://' . realpath($validationFile)]);
            if (!$validator->isValid()) {
                call_user_func($this->invalidCallback, $validator->getErrors());
            }
        }
        return json_decode($requestBody, true);
    }
}