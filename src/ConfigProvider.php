<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Yaml\Yaml;

class ConfigProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['config'] = function () {
            return Yaml::parseFile('config/config.yml');
        };
    }
}