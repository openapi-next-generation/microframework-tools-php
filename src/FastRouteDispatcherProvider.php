<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class FastRouteDispatcherProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple[Dispatcher::class] = function (Container $pimple) {
            $container = new \Pimple\Psr11\Container($pimple);
            return simpleDispatcher(function (RouteCollector $routes) use ($container) {
                require 'src/routes.php';
            });
        };
    }
}