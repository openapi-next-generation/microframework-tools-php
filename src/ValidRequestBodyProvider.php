<?php

namespace OpenapiNextGeneration\MicroframeworkToolsPhp;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ValidRequestBodyProvider implements ServiceProviderInterface
{
    protected $invalidCallback;

    public function __construct(callable $invalidCallback)
    {
        $this->invalidCallback = $invalidCallback;
    }

    public function register(Container $pimple)
    {
        $pimple[ValidRequestBody::class] = function () {
            return new ValidRequestBody($this->invalidCallback);
        };
    }
}